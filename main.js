function maintable_Resize()
{
    var maintable = document.getElementById('maintable');
    var width = window.innerWidth;
    var desktopWidth = 800;
    if ( width > desktopWidth )
    {
        document.body.className = 'clsDesktop';
        maintable.width = desktopWidth + 'px';
        document.body.style.marginLeft = ((width - desktopWidth) * 0.5) + 'px';
    }
    else
    {
        document.body.className = 'clsPhone';
        document.body.style.margin = '0';
        document.body.style.padding = '0';
    }
    //console.log(width);
}
maintable_Resize();
window.onresize = function()
{
    maintable_Resize();

    //console.log('outerWidth ' + window.outerWidth);
};